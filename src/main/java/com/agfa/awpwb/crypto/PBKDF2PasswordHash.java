package com.agfa.awpwb.crypto;

import java.math.BigInteger;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.security.spec.InvalidKeySpecException;
import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.PBEKeySpec;

public class PBKDF2PasswordHash {
    private static final String PBKDF2_ALGORITHM = "PBKDF2WithHmacSHA1";

    // The following constants may be changed without breaking existing hashes.
    private static final int SALT_BYTES = 64;
    private static final int HASH_BYTES = 64;
    private static final int PBKDF2_ITERATIONS = 20000;

    public static String createHash(String password, int saltBytes, int hashBytes, int iterations)
            throws NoSuchAlgorithmException, InvalidKeySpecException {
        return createHash(password.toCharArray(), saltBytes, hashBytes, iterations);
    }

    /**
     * Returns a salted PBKDF2 hash of the password.
     *
     * @param password the password to hash
     * @return a salted PBKDF2 hash of the password
     */
    public static String createHash(char[] password, int saltBytes, int hashBytes, int iterations)
            throws NoSuchAlgorithmException, InvalidKeySpecException {
        byte[] salt = new byte[saltBytes];

        byte[] hash = pbkdf2(password, salt, iterations, hashBytes);
        return toHex(hash);
    }

    /**
     * Computes the PBKDF2 hash of a password.
     *
     * @param password   the password to hash.
     * @param salt       the salt
     * @param iterations the iteration count (slowness factor)
     * @param bytes      the length of the hash to compute in bytes
     * @return the PBDKF2 hash of the password
     */
    private static byte[] pbkdf2(char[] password, byte[] salt, int iterations, int bytes)
            throws NoSuchAlgorithmException, InvalidKeySpecException {
        PBEKeySpec spec = new PBEKeySpec(password, salt, iterations, bytes * 8);
        SecretKeyFactory skf = SecretKeyFactory.getInstance(PBKDF2_ALGORITHM);
        return skf.generateSecret(spec).getEncoded();
    }

    private static String toHex(byte[] array) {
        BigInteger bi = new BigInteger(1, array);
        String hex = bi.toString(16);
        int paddingLength = (array.length * 2) - hex.length();
        if (paddingLength > 0)
            return String.format("%0" + paddingLength + "d", 0) + hex;
        else
            return hex;
    }

    public static void main(String[] args) {
        try {
            for(int i=0; i<10;i++) {
                System.out.println(PBKDF2PasswordHash.createHash("topsecret", SALT_BYTES, HASH_BYTES, PBKDF2_ITERATIONS));
            }

        } catch (Exception ex) {
            System.out.println("ERROR: " + ex);
        }
    }

}
